package mysqldatabasejava;

import java.sql.*;

public class ConnectManager {

	private static Connection con = null;
	private static PreparedStatement pStmt = null;
	private static ResultSet rs = null;
	private static ConnectManager instance = null;
	
	private ConnectManager() {
		
		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost/"
					+ "bewerbungfirmen?useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
						"root", "");
		} catch (SQLException e) {				
			System.err.println(e);
		}		
	}
	
	public static ConnectManager getInstanceOf() {
		if (instance == null) {
			instance = new ConnectManager();
		} 			
		return instance;
	}
	
	private String valuesToString(ResultSet rs) {
		String resu = "";
		
			try {
				while(rs.next()) {
				resu = resu + "                                    " + rs.getString("ID") + "   ||   " + rs.getString("Firmenname") + "   ||   " +
					rs.getString("Ansprechpartner") + "   ||   " + rs.getString("Link") + "   ||   " + rs.getString("VG") + "\n\n";
				}
			} catch (SQLException e) {
				System.err.println(e);
			}
					
		return resu;		
	}
	
	public String showAll() {
				
		try {
			pStmt = con.prepareStatement("SELECT * FROM companys");			
			rs = pStmt.executeQuery();
			
			//System.out.println(valuesToString(rs));				
						
		} catch (SQLException e) {			
			System.err.println(e);
		}	
		
		return valuesToString(rs);
		
	}
	
	public String searchFor(String columnName, String searchFor) {
				
		try {
			pStmt = con.prepareStatement("SELECT * FROM companys WHERE " + columnName + " = ?");
			pStmt.setString(1, searchFor);
			rs = pStmt.executeQuery();
			
			//System.out.println(valuesToString(rs));	
			
		} catch (SQLException e) {			
			System.err.println(e);
		}
		
		return valuesToString(rs);		
	}
	
	public void insertInTable(String firmName, String hrName, String link, String yesOrNo) {
		try {
			pStmt = con.prepareStatement("INSERT INTO companys (Firmenname, Ansprechpartner, Link, VG) "
					+ "VALUES (?,?,?,?) ");
			pStmt.setString(1, firmName);
			pStmt.setString(2, hrName);
			pStmt.setString(3, link);
			pStmt.setString(4, yesOrNo);
			pStmt.executeUpdate();
		} catch (SQLException e) {		
			System.err.println(e);
		}
	}
	
	public void deleteFromTable(String id) {
		String resu = "";
		
		try {
			pStmt = con.prepareStatement("DELETE FROM companys WHERE ID = ?");
			pStmt.setString(1, id);
			pStmt.executeUpdate();		
			
		} catch (SQLException e) {			
			System.err.println(e);
		}		
	}
	
	public void update(String id, String columnName, String newString) {
				
		try {
			System.out.println("updating");
			pStmt = con.prepareStatement("UPDATE companys SET " + columnName + " = ? WHERE id = ?");				
			pStmt.setString(1, newString);	
			pStmt.setString(2, id);
			
			pStmt.executeUpdate();		
			
		} catch (SQLException e) {			
			System.err.println(e);
		}			
	}
		
//	public static void main(String args[]) {
//		ConnectManager cm = new ConnectManager();
////		cm.insertInTable("Obi AG", "Janine Ruessel", "nichen@arcor.de", "ABSAGE");
////		cm.searchFor("Ansprechpartner", "Janine Ruessel");
////		cm.deleteFromTable("Firmenname", "Obi AG");
////		cm.update("9", "Firmenname", "BLA AG");
////		cm.deleteFromTable("1");
//		cm.showAll();	
//		
//	}
}
