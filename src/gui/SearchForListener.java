package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import mysqldatabasejava.ConnectManager;

public class SearchForListener implements ActionListener {
		
	private static ConnectManager _cm; 
	private static String _columnName;
	private static String _searchFor;
	
	
	
	public String alSearchFor(ConnectManager cm, String columnName, String searchFor) {
					
		_cm = cm;	
		_columnName = columnName;
		_searchFor = searchFor;
		String out = "";
		
		if (!searchFor.isEmpty()) {
			out = _cm.searchFor(columnName, searchFor);
		} 		
		
		return out;
			
	}	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		alSearchFor(_cm, _columnName, _searchFor);		
	}
	
}
