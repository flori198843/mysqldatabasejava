package mysqldatabasejava;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Audio {

	/* Audio for Button-click */	
	 
	private static void makeSound(String filename) {
		try {			
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(filename).getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();		
		} catch (IOException | UnsupportedAudioFileException | LineUnavailableException ex) {		
			ex.printStackTrace();
		}
	}
	
	public static void buttonAddSound() {
		String soundName = "audio/electronic_stapler.wav";  
		makeSound(soundName);
	}
	
	public static void buttonDeleteSound() {
		String soundName = "audio/bumerang.wav";  
		makeSound(soundName);
	}
	
	public static void buttonShowAllSound() {
		String soundName = "audio/tape-measure-1.wav";  
		makeSound(soundName);
	}
	
	public static void buttonFindSound() {
		String soundName = "audio/sound69.wav";  
		makeSound(soundName);
	}
	
	public static void buttonUpdateSound() {
		String soundName = "audio/sound80.wav";  
		makeSound(soundName);
	}
	
	public static void buttonResetSound() {
		String soundName = "audio/sound98.wav";  
		makeSound(soundName);
	}
	
}