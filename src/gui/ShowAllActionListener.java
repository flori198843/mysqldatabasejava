package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import mysqldatabasejava.ConnectManager;

public class ShowAllActionListener implements ActionListener {
	
	private static ConnectManager _cm; 
				
	public String alShowAll(ConnectManager cm) {
					
		_cm = cm;	
		
		return _cm.showAll();		
	}	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		alShowAll(_cm);		
	}
	
}
