package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import mysqldatabasejava.ConnectManager;

public class DeleteActionListener implements ActionListener {
	
	private static ConnectManager _cm; 
	private static String _id;
			
			
	public static void alDelete(ConnectManager cm, String id) {
		
		_id = id;
		
		_cm = cm;					
		_cm.deleteFromTable(id);		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		alDelete(_cm, _id);		
	}
	
}
