package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import mysqldatabasejava.ConnectManager;

public class InsertActionListener implements ActionListener {
	
	private static ConnectManager _cm; 
	private static String _firmName;
	private static String _mrMrs;
	private static String _hrName;
	private static String _link;
	private static String _yesOrNo;
	
		
			
	public static int alInstert(ConnectManager cm, String firmName, String mrMrs, String hrName, String link, String yesOrNo) {
		
		_firmName = firmName;
		_mrMrs = mrMrs;
		_hrName = hrName;
		_link = link;
		_yesOrNo = yesOrNo;
		_cm = cm;
		
		int err = 0;
		
		if (firmName.isEmpty() || hrName.isEmpty() || link.isEmpty() || yesOrNo.isEmpty()) {
			System.err.println("ERROR: At least one field is empty!");
			err = -1;
		} else {
			_cm.insertInTable(firmName, mrMrs + " " + hrName, link, yesOrNo);
			System.out.println("Added into DB!");
		}			
		return err;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		alInstert(_cm, _firmName, _mrMrs, _hrName, _link, _yesOrNo);		
	}
	
}
