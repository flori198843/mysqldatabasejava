package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import mysqldatabasejava.Audio;
import mysqldatabasejava.ConnectManager;

public class GUI {
	
	private static final int numTextFields = 5;
	private static final int numButtons = 2;
	
	private JTextField textFields[] = new JTextField[numTextFields];
	private JLabel labels[] = new JLabel[numTextFields+1];
	private JButton buttons[][] = new JButton[numButtons][numButtons];
	private JButton reset = new JButton();
	private JButton btnShowAll = new JButton();
	
	private JTextArea showPanel = new JTextArea();	
	private JPanel panel = new JPanel(null);
	private JFrame mainFrame = new JFrame();
	
	private JComboBox comboHrrFr = new JComboBox();
	
	private ConnectManager cm = ConnectManager.getInstanceOf();
		
	public GUI() {
				
		mainFrame.setTitle("DB Firmen");
		mainFrame.setVisible(true);	
		mainFrame.setSize(800, 900);		
		mainFrame.setResizable(false);				
		
		createTextFields();
		createLabels();
		createButtons();		
				
		mainFrame.add(panel);
		
		mainFrame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}			
		});		
		
		showPanel.setText(cm.showAll());
		
		comboHrrFr.setBounds(100, 210, 80, 30);
		comboHrrFr.addItem("Herr");
		comboHrrFr.addItem("Frau");
		comboHrrFr.setSelectedItem(0);
		
		panel.add(comboHrrFr);
	}
	
	/* Index goes from up to down */
	
	private void createTextFields() {
		for (int i = 0; i < numTextFields; i++) {	
			textFields[i] = new JTextField();
			textFields[i].setBounds(200, 50 + (i*80), 300, 30);
			panel.add(textFields[i]);
		}	
			
		showPanel.setBounds(40, 450, 700, 380);
		showPanel.setEditable(false);
		panel.add(showPanel);		
	}	
	
	private void createLabels() {
		for (int i = 0; i < numTextFields; i++) {	
			labels[i] = new JLabel();
			labels[i].setBounds(200, 20 + (i*80), 300, 30);			
		}	
		
		labels[0].setText("ID:");
		labels[1].setText("Firmenname:");
		labels[2].setText("Ansprechpartner:");
		labels[3].setText("Link:");
		labels[4].setText("VG:");	
		
		labels[5] = new JLabel();
		labels[5].setBounds(100, 180, 80, 30);
		labels[5].setText("Anrede:");
		
		for (int i = 0; i < numTextFields+1; i++) {				
			panel.add(labels[i]);		
		}			
	}
	
	/* Buttons index: [column] [row] */
	
	private void createButtons() {		
		for (int i = 0; i < numButtons; i++) {	
			for (int j = 0; j < numButtons; j++) {
				buttons[i][j] = new JButton();					
				buttons[i][j].setBounds(550 + (i*90), 50 + (j*50) , 80, 40);						
				panel.add(buttons[i][j]);				
			}		
		}	
		btnShowAll.setBounds(550, 150, 170, 40);
		btnShowAll.setText("Show all");
		panel.add(btnShowAll);
		
		reset.setBounds(100, 50, 80, 30);
		reset.setText("Reset");
		panel.add(reset);
		
		buttons[0][0].setText("ADD");
		buttons[0][1].setText("DEL");
		buttons[1][0].setText("FIND");
		buttons[1][1].setText("UPD");
		
		buttons[1][1].setEnabled(false);
		
		buttons[0][0].addActionListener(new InsertActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (alInstert(cm, textFields[1].getText(), comboHrrFr.getSelectedItem().toString(), textFields[2].getText(), textFields[3].getText(), textFields[4].getText()) == 0) {
					textFields[0].setText("");		
					textFields[1].setText("");	
					textFields[2].setText("");	
					textFields[3].setText("");	
					textFields[4].setText("");	
					showPanel.setText(cm.showAll());
					Audio.buttonAddSound();
				}				
			}
		});		
		
		buttons[0][1].addActionListener(new DeleteActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				alDelete(cm, textFields[0].getText());	
				showPanel.setText(cm.showAll());
				Audio.buttonDeleteSound();
			}
		});
		
		buttons[1][0].addActionListener(new SearchForListener() {
			
			String[] columnNames = {"ID", "Firmenname", "Ansprechpartner", "Link", "VG"};
			@Override
			public void actionPerformed(ActionEvent e) {	
				showPanel.setText("");			
				for (int i = 0; i < columnNames.length; i++) {
					if (!textFields[i].getText().isEmpty()) {					
						showPanel.setText(alSearchFor(cm, columnNames[i], textFields[i].getText()));					
					} 					
				}
				
				if (!showPanel.getText().isEmpty() && !textFields[0].getText().isEmpty() && textFields[1].getText().isEmpty()
						&& textFields[2].getText().isEmpty() && textFields[3].getText().isEmpty() && textFields[4].getText().isEmpty()) {
					buttons[1][1].setEnabled(true);
				
			}
				Audio.buttonFindSound();
			}
		});
		
		buttons[1][1].addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				String title = comboHrrFr.getSelectedItem().toString();
				String[] param = {textFields[1].getText(), title + " " + textFields[2].getText(), textFields[3].getText(), textFields[4].getText()};
				String[] columnNames = {"Firmenname", "Ansprechpartner", "Link", "VG"};				
				
				for (int i = 0; i < param.length; i++) {
					if (!param[i].isEmpty()) {
						cm.update(textFields[0].getText(), columnNames[i], param[i]);
						showPanel.setText(cm.searchFor("ID", textFields[0].getText()));
						Audio.buttonUpdateSound();
					}
				}				
			}
		});
		
		btnShowAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showPanel.setText(cm.showAll());	
				buttons[1][1].setEnabled(false);
				Audio.buttonShowAllSound();
			}
		});
		
		reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < numTextFields; i++) {
					textFields[i].setText("");
				}
				showPanel.setText(cm.showAll());
				buttons[1][1].setEnabled(false);	
				comboHrrFr.setSelectedItem("Herr");
				Audio.buttonResetSound();
			}
		});
	}
		
	public static void main(String args[]) {
		new GUI();		
	}
	
}
